// Copyright 2020 - 2022 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <random>
#include <vector>
#include <iostream>
#include <algorithm>

#include <rand/random_sequence.hpp>
#include <str/distance.hpp>

#include <options.hpp>
#include <tasks.hpp>
#include <edit_information.hpp>

using namespace aed;

int main(int argn, char **argv) {
    Options opts(argn, argv);
    if (opts.verbosity == Verbosity::VerbosityMax) {
        opts.show_options(std::cout);
    }
    auto base_generator = ctl::make_uniform_sequence_distribution<char>(opts.alphabet);
    std::mt19937 rand_engine(opts.seed);

    if (opts.subprogram == "min-spread-f" or opts.subprogram == "cost-f") {
        int output_type = DifferenceCost;
        if (opts.subprogram == "min-spread-f") {
            output_type = DifferenceMinSpread;
        }
        std::vector<int> results;
        EditDistanceLinearSpaceShortInfo alg(opts.n, opts.n, opts.w);
        sample_edit_distance_with_mutation(opts.n, opts.k, 1, output_type, results, alg, base_generator, rand_engine);
        std::cout << "diff" << std::endl;
        for (auto r: results) {
            std::cout << r << std::endl;
        }
        exit(0);
    }

    if (opts.subprogram == "wide-alphabet") {
        long alphabet_size = std::stol(opts.alphabet);
        auto distribution = ctl::make_uniform_integer_distribution<long>(0, alphabet_size - 1);

        if (opts.algorithm == EDAlgorithm::HammingDistance) {
            std::vector<size_t> v;
            auto alg = ctl::make_hamming_distance_algorithm();
            sample_edit_distance_generic<std::vector<long>>(opts.n, opts.k, v, alg, distribution, rand_engine);
            std::cout << "HD" << std::endl;
            std::copy(v.cbegin(), v.cend(), std::ostream_iterator<int>(std::cout, "\n"));
            exit(0);
        }

        if (opts.algorithm == EDAlgorithm::LinearSpaceBandwidth) {
            std::vector<size_t> v;
            auto alg = ctl::make_band_linear_alg(opts.n, opts.n, opts.n / 2 + 1);
            sample_edit_distance_generic<std::vector<long>>(opts.n, opts.k, v, alg, distribution, rand_engine);
            std::cout << "ED" << std::endl;
            std::copy(v.cbegin(), v.cend(), std::ostream_iterator<int>(std::cout, "\n"));
        } else {
            EditDistanceLinearSpaceShortInfo alg(opts.n, opts.n, opts.w);
            std::vector<InfoShort<size_t>> results;
            sample_edit_distance_generic<std::vector<long>>(opts.n, opts.k, results, alg, distribution, rand_engine);
            std::cout << "distance,count,min_sub,max_sub,min_spread" << std::endl;
            std::cout << "distance,count,min_sub,max_sub,min_spread" << std::endl;
            for (InfoShort<size_t> r: results) {
                std::cout << r << std::endl;
            }
        }
        exit(0);
    }

    if (opts.subprogram == "test") {
        std::string test_string = "Non-string iterators";
        std::cout << "Testing: " << test_string << std::endl;
        std::vector<int> a = {1,2,3};
        std::vector<int> b = {1,5,2};
        std::cout << ctl::hamming_distance(a.begin(), a.end(), b.begin()) << std::endl;
        exit(0);
    }


    // TODO: Extremely suboptimal code (see replicated call to sample_edit_distance) how could solve this?
    if (opts.algorithm == EDAlgorithm::LinearSpaceInformation) { // full information algorithm
        EditDistanceLinearSpaceShortInfo alg(opts.n, opts.n, opts.w);
        std::vector<InfoShort<size_t>> results;
        sample_edit_distance(opts.n, opts.k, results, alg, base_generator, rand_engine);
        std::cout << "distance,count,min_sub,max_sub,min_spread" << std::endl;
        for (InfoShort<size_t> r: results) {
            std::cout << r << std::endl;
        }
    } else {
        std::vector<size_t> v;

        if (opts.algorithm == EDAlgorithm::LinearSpaceBandwidth) { // linear space algorithm distance only
            auto alg = ctl::make_band_linear_alg(opts.n, opts.n, opts.n / 2 + 1);
            sample_edit_distance(opts.n, opts.k, v, alg, base_generator, rand_engine);
        } else { // quadratic space algorithm distance only
            auto alg = ctl::make_wf_alg(opts.n, opts.n);
            sample_edit_distance(opts.n, opts.k, v, alg, base_generator, rand_engine);
        }
        std::cout << "ED" << std::endl;
        std::copy(v.cbegin(), v.cend(), std::ostream_iterator<int>(std::cout, "\n"));
    }
    return 0;
}
