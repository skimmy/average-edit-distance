// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <iterator>
#include <vector>
#include <random>
#include <edit_information.hpp>
#include <util.hpp>

using namespace aed;

std::vector<std::pair<std::string, std::string>> test_pairs = {
        {"GGGGGGGGG",             "GGGGGGGGG"},
        {"ACGTTTA",               "ACGTGAT"},
        {"AAAAAAAAAAAAAAAAAAAAA", "GGGGGGGGGGGGGGGGGGGGG"},
        {"AGTGTGTGT",             "GTGTGTGTA"},
        {"ACTCTACTCTACTCT",       "CTCTCTCTCTCTGGG"},
        {"CACCCGTAGC",            "AGGGACCTTA"},
        {"CCG",                   "AGC"},
        {"ATA",                   "AAT"},
        {"ACGTACGT",              "CGTA"},
};

void test_mutation(long seed) {
    std::mt19937 rand_engine(seed);
    for (auto p : test_pairs) {
        std::cout << p.first << std::endl;
        size_t pos = random_mutation(p.first, rand_engine);
        std::cout << p.first << " (" << pos << ")" << std::endl;
        std::cout << std::endl;
    }
}

void test_info_short() {
    InfoShort<> a, b, c, d;
    b.cost = 1;
    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "c = " << d << std::endl;
    std::cout << "d = Update(a,b,c,1)" << std::endl;
    d.UpdateFromPrecedingCells(a, b, c, 1, 2, 3);
    std::cout << "d = " << d << std::endl;
}

void test_info_short_alg(const std::string &x, const std::string &y) {
    size_t large_edge = std::max(x.size(), y.size());
    EditDistanceLinearSpaceShortInfo alg(large_edge, large_edge);

    std::cout << std::endl;
    std::cout << "calculate with iterator\nx = " << x << "\ny = " << y << std::endl;
    auto result = alg(x.cbegin(), x.cend(), y.cbegin(), y.cend());
    std::cout << result << std::endl;

    std::cout << std::endl;
    std::cout << "string exchanged\nx = " << y << "\ny = " << x << std::endl;
    result = alg(y, x);
    std::cout << result << std::endl;

    std::cout << std::endl;
    result = alg(x.crbegin(), x.crend(), y.crbegin(), y.crend());
    std::cout << "reverse test" << std::endl << "x = ";
    std::copy(x.crbegin(), x.crend(), std::ostream_iterator<char>(std::cout));
    std::cout << std::endl << "y = ";
    std::copy(y.crbegin(), y.crend(), std::ostream_iterator<char>(std::cout));
    std::cout << std::endl;
    std::cout << result << std::endl;
}

void test_info_with_bandwidth(const std::string &x, const std::string &y) {
    size_t n = x.size();
    size_t m = y.size();
    std::vector<size_t> ws({0, 2, n});
    for (size_t w : ws) {
        EditDistanceLinearSpaceShortInfo alg(n, m, w);
        std::cout << "\nBandwidth: w = " << w << "\nx = " << x << "\ny = " << y << std::endl;
        auto result = alg(x, y);
        std::cout << result << std::endl;
    }
    std::cout << std::endl;
}

int main() {
//    std::cout << "AED Testing" << std::endl;
//    std::cout << std::endl << "Testing 'InfoShort'" << std::endl;
//    test_info_short();
//    std::cout << std::endl << "Testing 'InfoShort Algorithm'" << std::endl;
//    for (auto p : test_pairs) {
//        test_info_short_alg(p.first, p.second);
//    }
//    std::cout << std::endl << "Testing 'InfoShort Algorithm' with Bandwidth" << std::endl;
//    for (auto p : test_pairs) {
//        test_info_with_bandwidth(p.first, p.second);
//    }
    test_mutation(12321);
}