// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef AED_UTIL_HPP
#define AED_UTIL_HPP

#include <string>
#include <vector>
#include <random>

template<class ContT, class RndEng>
size_t random_mutation(ContT &v, RndEng &rg) {
    std::string alphabet = "ACGT";
    std::vector<size_t> pV = {1, 2, 3};
    // Generate position in v and random mutation
    auto posGenerator = std::uniform_int_distribution<size_t>(0, v.size() - 1);
    auto mutGenerator = std::uniform_int_distribution<size_t>(1, 3);
    size_t position = posGenerator(rg);
    size_t mutation = mutGenerator(rg);
    // Generate mutation adding 1, 2, or 3 to the index of base in the alphabet
    v[position] = alphabet[(alphabet.find(v[position]) + mutation) % alphabet.size()];
    return position;
}

#endif //AED_UTIL_HPP
