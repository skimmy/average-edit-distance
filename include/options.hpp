// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef AED_OPTIONS_HPP
#define AED_OPTIONS_HPP


#include <cstdlib>
#include <iostream>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

namespace aed {

enum Verbosity { VerbosityNone = 0, VerbosityLow, VerbosityHigh, VerbosityMax};
enum EDAlgorithm { WagnerFisher = 0, LinearSpaceBandwidth, LinearSpaceInformation, HammingDistance=9};

class Options {
public:
    // the subprogram to run (first argument, optional)
    std::string subprogram {""};

    // parameters for simulations
    std::size_t n = 128;
    std::size_t k = 100;
    std::size_t w = -1;
    int algorithm = WagnerFisher;
    long seed = 0;
    std::string alphabet {"ACGT"};

    // program behavior parameters
    int verbosity = Verbosity::VerbosityNone;

    Options(int argn, char** argv);

    void
    show_options(std::ostream & os);

private:
    void
    print_usage(std::ostream &os);

    void adjust_default();
};

}

#endif //AED_OPTIONS_HPP
