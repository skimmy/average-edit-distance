// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef AED_EDIT_INFORMATION_HPP
#define AED_EDIT_INFORMATION_HPP

#include <cstdlib>
#include <algorithm>
#include <iostream>

#include <data_structure/matrix.hpp>

namespace aed {

/// Class for the information to be computed by a linear space
/// algorithm for the edit distance.
/// \tparam SizeT the type for distance-related information
template<typename SizeT = size_t>
class InfoShort {
public:
    SizeT cost;
    double count;
    SizeT min_sub;
    SizeT max_sub;
    SizeT min_spread;

    InfoShort()
            : cost(0), count(1), min_sub(0), max_sub(0), min_spread(0) {}

    InfoShort(const InfoShort<SizeT> &other) :
            cost(other.cost), count(other.count), min_sub(other.min_sub),
            max_sub(other.max_sub), min_spread(other.min_spread) {}

    /// Updates information from previous cells. The convention is that the
    /// three cells are, respectively: diagonal, above, left.
    /// \param a cell diagonal preceding
    /// \param b cell above
    /// \param c cell on the left
    /// \param delta hamming distance between current characters
    void UpdateFromPrecedingCells(const InfoShort<SizeT> &a,
                                  const InfoShort<SizeT> &b,
                                  const InfoShort<SizeT> &c,
                                  SizeT delta,
                                  size_t i,
                                  size_t j) {
        // convention is a->diagonal, b->vertical v->horizontal

        // update distance
        cost = std::min({a.cost + delta, b.cost + 1, c.cost + 1});

        // match case
        if (cost == a.cost and delta == 0) {
            count = a.count;
            min_sub = a.min_sub;
            max_sub = a.max_sub;
            min_spread = a.min_spread;
            if (b.cost == cost - 1) { // DEL is also ok
                count += b.count;
                min_sub = std::min(min_sub, b.min_sub);
                max_sub = std::max(max_sub, b.max_sub);
                min_spread = std::min(min_spread, b.min_spread);
            }
            if (c.cost == cost - 1) { // INS is also ok
                count += c.count;
                min_sub = std::min(min_sub, c.min_sub);
                max_sub = std::max(max_sub, c.max_sub);
                min_spread = std::min(min_spread, c.min_spread);
            }
        } else { // Not MATCH
            // we follow the backtracking that minimizes the cost
            SizeT min_back = std::min({a.cost, b.cost, c.cost});
            if (a.cost == min_back) { // SUB is optimal
                count = a.count;
                min_sub = a.min_sub + 1;
                max_sub = a.max_sub + 1;
                min_spread = a.min_spread;
                if (b.cost == min_back) { // DEL also optimal
                    count += b.count;
                    min_sub = std::min(min_sub, b.min_sub);
                    max_sub = std::max(max_sub, b.max_sub);
                    min_spread = std::min(min_spread, b.min_spread);
                }
                if (c.cost == min_back) { // INS also optimal
                    count += c.count;
                    min_sub = std::min(min_sub, c.min_sub);
                    max_sub = std::max(max_sub, c.max_sub);
                    min_spread = std::min(min_spread, c.min_spread);
                }
            } else { // Not MATCH, not SUB
                if (b.cost == c.cost) { // DEL and INS optimal
                    count = b.count + c.count;
                    min_sub = std::min(b.min_sub, c.min_sub);
                    max_sub = std::max(b.max_sub, c.max_sub);
                    min_spread = std::min(b.min_spread, c.min_spread);
                } else {
                    if (b.cost < c.cost) { // DEL only optimal
                        count = b.count;
                        min_sub = b.min_sub;
                        max_sub = b.max_sub;
                        min_spread = b.min_spread;
                    } else { // INS only optimal
                        count = c.count;
                        min_sub = c.min_sub;
                        max_sub = c.max_sub;
                        min_spread = c.min_spread;
                    }
                }

            }
        }
        SizeT current_spread = i > j ? i - j : j - i;
        min_spread = std::max(min_spread, current_spread);

    }
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const InfoShort<T> info) {
    return (os << info.cost << "," << info.count << "," << info.min_sub << "," << info.max_sub << ","
               << info.min_spread);
}

class EditDistanceLinearSpaceShortInfo {
private:
    ctl::_2D_array<InfoShort<size_t>> dp_matrix;
    size_t n;
    size_t m;
    size_t w;
    size_t Inf;

public:
    EditDistanceLinearSpaceShortInfo(size_t _n, size_t _m)
            : dp_matrix{2, _m + 1}, n{_n}, m{_m}, w{2 * (std::max(_n, _m)) + 1}, Inf{_n + _m + 1} {}

    EditDistanceLinearSpaceShortInfo(size_t _n, size_t _m, size_t _w)
            : dp_matrix{2, _m + 1}, n{_n}, m{_m}, w{_w}, Inf{_n + _m + 1} {}

    template<typename IterT>
    InfoShort<size_t> operator()(IterT b1, IterT e1, IterT b2, IterT e2) {
        // we prevent exceeding matrix indexes by stopping computation
        // within the limits of the matrix if provided iterators exceed
        // such limit (see the limits of the for loops below)
        size_t n1 = std::distance(b1, e1);
        size_t n2 = std::distance(b2, e2);
        init();
        for (size_t i = 1; i <= std::min(n, n1); ++i) {
            size_t j = (i <= w) ? 1 : i - w; // with size_t we cannot use i-w because it is unsigned
            if (j - 1 <= m) {
                dp_matrix(1, j - 1).cost = (i <= w) ? i : Inf;
                dp_matrix(1, j - 1).count = 1;
                dp_matrix(1, j - 1).min_sub = 0;
                dp_matrix(1, j - 1).max_sub = 0;
                dp_matrix(1, j - 1).min_spread = i;
            }
//            for (int iii = 0; iii < j-1; iii++) {std::cout << "\t";}
//            std::cout << dp_matrix(1, j - 1).cost;

            for (; j <= std::min({m, i + w, n2}); ++j) {
                size_t delta = (b1[i - 1] == b2[j - 1]) ? 0 : 1;
                dp_matrix(1, j).UpdateFromPrecedingCells(
                        dp_matrix(0, j - 1), dp_matrix(0, j), dp_matrix(1, j - 1), delta, i, j);
//                std::cout <<"\t" << dp_matrix(1, j).cost;

            }
            if (i + w < m) {
                dp_matrix(1, i + 1 + w).cost = Inf;
//                std::cout << "\t" << dp_matrix(1, i + 1 + w).cost;
            }
//            std::cout << std::endl;
            dp_matrix.swap_rows(0, 1);
        }
        return dp_matrix(0, std::min(m, n2));
    }

    template<typename ContT>
    InfoShort<size_t> operator()(const ContT &c1, const ContT &c2) {
        return (*this)(c1.cbegin(), c1.cend(), c2.cbegin(), c2.end());
    }


private:
    void init() {
        for (size_t j = 0; j <= std::min(w, m); ++j) {
            dp_matrix(0, j).cost = j;
            dp_matrix(0, j).count = 1;
            dp_matrix(0, j).max_sub = 0;
            dp_matrix(0, j).min_sub = 0;
            dp_matrix(0, j).min_spread = j;
//            std::cout << dp_matrix(0, j).cost << "\t";
        }
        for (size_t j = w + 1; j <= m; ++j) {
            dp_matrix(0, j).cost = Inf;
            dp_matrix(0, j).count = 1;
            dp_matrix(0, j).max_sub = 0;
            dp_matrix(0, j).min_sub = 0;
            dp_matrix(0, j).min_spread = j;
//            std::cout << dp_matrix(0, j).cost << "\t";
        }
//        std::cout << std::endl;
    }

};

#endif //AED_EDIT_INFORMATION_HPP

}
