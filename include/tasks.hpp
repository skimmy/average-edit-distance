// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef AED_PRJ_TASKS_HPP
#define AED_PRJ_TASKS_HPP

#include <cstdlib>
#include <utility>
#include <string>
#include <algorithm>
#include <iostream>

#include <util.hpp>

namespace aed {

constexpr int DifferenceCost = 0;
constexpr int DifferenceMinSpread = 1;

using StringPair = std::pair<std::string, std::string>;

/// \brief Performs sampling of edit distance between same size strings
///
/// \tparam ContT container for results must support std::back_inserter
/// \tparam Alg algorithm to compute edit distance must support operator () with 4 iterators
/// \tparam BaseGen distribution to generate random characeters must support operator () with lengh
/// \tparam RndEng random engine used by the standard C++ library
/// \param n the length of the strings
/// \param k the number of samples
/// \param results the container where resulting distances are put
/// \param algorithm the algorithm used to compute the distance
/// \param generator random generator for the strings
/// \param rd random device used with generator
template<class ContT, class Alg, class BaseGen, class RndEng>
void sample_edit_distance(size_t n, size_t k, ContT &results, Alg &algorithm, BaseGen &generator, RndEng &rd) {
    for (size_t i = 0; i < k; ++i) {
        // make a pair of empty string and fill randomly
        StringPair string_pair = std::make_pair(std::string(), std::string());
        generator(n, std::back_inserter(string_pair.first), rd);
        generator(n, std::back_inserter(string_pair.second), rd);
        // compute distance and add to the result container
        auto distance = algorithm(string_pair.first.cbegin(), string_pair.first.cend(),
                                  string_pair.second.cbegin(), string_pair.second.cend());
        results.push_back(distance);
    }
}

template<class ContT, class Alg, class BaseGen, class RndEng>
void sample_edit_distance_with_mutation(
        size_t n, size_t k, size_t mutations, int result_type, ContT &results, Alg &algorithm, BaseGen &generator, RndEng &rd) {
    auto randomString = std::uniform_int_distribution<size_t>(0, 1);
    for (size_t i = 0; i < k; ++i) {
        // make a pair of empty string and fill randomly
        StringPair string_pair = std::make_pair(std::string(), std::string());
        generator(n, std::back_inserter(string_pair.first), rd);
        generator(n, std::back_inserter(string_pair.second), rd);

        // compute distance and add to the result container
        auto distance = algorithm(string_pair.first.cbegin(), string_pair.first.cend(),
                                  string_pair.second.cbegin(), string_pair.second.cend());

        // perform mutations on one (but not both) of the string
        // TODO use 'mutation' parameter to control the number of mutations
        if (randomString(rd)) {
            random_mutation(string_pair.second, rd);
        } else {
            random_mutation(string_pair.first, rd);
        }

        auto distance_mutation = algorithm(string_pair.first.cbegin(), string_pair.first.cend(),
                                           string_pair.second.cbegin(), string_pair.second.cend());

        int diff = static_cast<int>(distance.cost) - static_cast<int>(distance_mutation.cost);
        if (result_type == DifferenceMinSpread) {
            diff = static_cast<int>(distance.min_spread) - static_cast<int>(distance_mutation.min_spread);
        }
        results.push_back(diff);
    }
}

template<class IterT, class ContT, class Alg, class BaseGen, class RndEng>
void sample_edit_distance_generic(size_t n, size_t k, ContT &results, Alg &algorithm, BaseGen &generator, RndEng &rd) {
    for (size_t i = 0; i < k; ++i) {
        // make a pair of empty sequences and fill randomly
        std::pair<IterT, IterT> sequence_pair = std::make_pair(IterT(), IterT());
        generator(n, std::back_inserter(sequence_pair.first), rd);
        generator(n, std::back_inserter(sequence_pair.second), rd);
        // // compute distance and add to the result container
        auto distance = algorithm(sequence_pair.first.cbegin(), sequence_pair.first.cend(),
                                  sequence_pair.second.cbegin(), sequence_pair.second.cend());
        results.push_back(distance);
    }
}

} // namespace aed

#endif //AED_PRJ_TASKS_HPP
