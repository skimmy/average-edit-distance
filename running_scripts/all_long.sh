#!/bin/zsh

OUT_DIR=~/Temporary

TIMESTAMP=$(date "+%Y%m%d")
SEED=2022
time ./aed -n 512 -k 2097152 -a 2 -s ${SEED} > ${OUT_DIR}/EDLinInfo_n512_k2M_s2022_"${TIMESTAMP}"

TIMESTAMP=$(date "+%Y%m%d")
SEED=2202
time ./aed -n 1024 -k 524288 -a 2 -s ${SEED} > ${OUT_DIR}/EDLinInfo_n1024_k512k_s2202_"${TIMESTAMP}"

TIMESTAMP=$(date "+%Y%m%d")
SEED=220282
time ./aed -n 2048 -k 131072 -a 2 -s ${SEED} > ${OUT_DIR}/EDLinInfo_n2048_k128k_s220282_"${TIMESTAMP}"

TIMESTAMP=$(date "+%Y%m%d")
SEED=820222
time ./aed -n 4096 -k 32768 -a 2 -s ${SEED} > ${OUT_DIR}/EDLinInfo_n4096_k32k_s8202222_"${TIMESTAMP}"

TIMESTAMP=$(date "+%Y%m%d")
SEED=222028
time ./aed -n 8192 -k 8192 -a 2 -s ${SEED} > ${OUT_DIR}/EDLinInfo_n8192_k8k_s222028_"${TIMESTAMP}"

TIMESTAMP=$(date "+%Y%m%d")
SEED=822202
time ./aed -n 16384 -k 2048 -a 2 -s ${SEED} > ${OUT_DIR}/EDLinInfo_n16384_k2k_s822202_"${TIMESTAMP}"
