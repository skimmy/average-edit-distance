#! /bin/zsh

algorithm=2
k=1
seed=22

for size in 512 1024 #2048 4096
do
    # Doesn't work on MacOS due to %N in 'date' command (sic!)
    start=$(date +"%s%N")
    ./aed -n ${size} -k ${k} -a ${algorithm} -s ${seed} > /dev/null
    end=$(date +"%s%N")
    msecs=$(( (end - start) / 100000000 ))
    printf "n = %-5s %10s ms/pair\n" ${size} ${msecs}
done
