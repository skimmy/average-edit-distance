#!/bin/zsh

OUT_DIR=~/Temporary

NS=(1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576)
KS=(524288 131072 32768 8192 2048 512 128 32 8 8 8)
#KS=(100 100 100 100 100)
WS=(61 100 161 260 420 680 1100 1780 2880 4660 7540)
SEEDS=(2202 220282 820222 222028 822202 86723 63556 270131 441262 882425 6758401)

for i in {1..11}
do
  printf "%d\tn = %d\t w = %d\n" "$i" "${NS[$i]}" "${WS[$i]}"
  TIMESTAMP=$(date "+%Y%m%d")
  SEED=${SEEDS[$i]}
  N=${NS[$i]}
  K=${KS[$i]}
  W=${WS[$i]}
  time ./aed -n "${N}" -k "${K}" -a 2 -w "${W}" -s "${SEED}" > "${OUT_DIR}/EDLinInfo_n${N}_w${W}_k${K}_s${SEED}_${TIMESTAMP}".csv
done
