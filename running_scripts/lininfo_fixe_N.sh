#!/bin/zsh

#SAMPLES=1
SAMPLES=1000
SEED=2202
OUT_DIR=/tmp

for SIZE in 32 64 128 256
do
  TIMESTAMP=$(date "+%Y%m%d")
  ./aed -n ${SIZE} -k ${SAMPLES} -a 2 -s ${SEED} > "${OUT_DIR}/EDLinInfo_n${SIZE}_k${SAMPLES}_s${SEED}_${TIMESTAMP}.csv"
done