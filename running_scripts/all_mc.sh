#!/bin/zsh

out_dir=~/Temporary

ns=(256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144)
#ks=(8388608 2097152 524288 131072 32768 8192 2048 512 128 32 8)
ks=(10 10 10 10 10 10 10 10 10 10 10)
seeds=(8068838 2022 2202 220282 8202222 222028 822202 86723 63556 270131 441262)
sigma=ACGT

for i in {1..11}
do
  printf "%d\tn = %d" "$i" "${ns[$i]}"
  timestamp=$(date "+%Y%m%d")
  seed=${seeds[$i]}
  n=${ns[$i]}
  k=${ks[$i]}
  time ./aed -n "${n}" -k "${k}" -a 2 -s "${seed}" --alphabet "${sigma}" > "${out_dir}/EDLinInfo_n${n}_k${k}_s${seed}_${timestamp}".csv
done
