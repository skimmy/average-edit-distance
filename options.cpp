// Copyright 2020 - 2022 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "include/options.hpp"

namespace aed {

Options::Options(int argn, char **argv) {
    if (argn > 1 and argv[1][0] != '-') {
        subprogram = argv[1];
    }
    po::options_description desc("All options");
    desc.add_options()
            ("length,n", po::value<size_t>(&n), // -n, --length
             "Size first string (if m is specified) or both string otherwise")

            ("samples,k", po::value<size_t>(&k), // -k, --samples
             "Number of samples (e.g., string pairs) to perform")

            ("algorithm,a", po::value<int>(&algorithm), // -a, --algorithm
             "Algorithm used to perform simulation: 0=WF (default), 1=LinBand, 2=LinInfo, 9=Hamming")

            ("bandwidth,w", po::value<size_t>(&w), // -w, --bandwidth
             "Value of the bandwidth (equals n if not specified)")

            ("alphabet", po::value<std::string>(&alphabet), // --alphabet
             "Alphabet to be used for random symbol generation (default 'ACGT') or alphabet size.")

            ("seed,s", po::value<long>(&seed), // -s, --seed
             "Seed for random generations")

            ("verbosity,v", po::value<int>(&verbosity), // -v, --verbosity
             "Verbosity level of the program output (integer)")

            ("help,h", "Show help message"); // -h, --help


    // invoke boost command line parser
    po::variables_map vm;
    po::store(po::parse_command_line(argn, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help") > 0) {
        print_usage(std::cout);
        std::cout << desc << std::endl;
        exit(0);
    }
    adjust_default();
}

void Options::adjust_default() {
    if (w == -1) {
        w = n;
    }
}

void Options::show_options(std::ostream &os) {
    os << "Showing options" << std::endl;
    os << "---------------" << std::endl;
    os << "Subprogram: " << subprogram << std::endl;
    os << "n " << n << std::endl;
    os << "k " << k << std::endl;
    os << "w " << w << std::endl;
    os << std::endl;
    os << "seed " << seed << std::endl;
    os << "verbosity " << verbosity << std::endl;
}

void Options::print_usage(std::ostream &os) {
    os << "\nUsage:\n";
    os << "\taed [task] [options]\n\n";
    os << "Allowed task:\n";
    os << "  [omitted]:      Computes the edit distance using the selected algorithm\n";
    os << "  cost-f:         Computes the difference in cost due to mutations\n";
    os << "  min-spread-f:   Computes the difference in minimum spread due to mutations\n";
    os << "  wide-alphabet:  Computes the edit distance using wide alphabets (--alphabet integer needed)\n";
    os << "\n";
}

}


