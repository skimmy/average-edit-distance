Average Edit Distance (AED)
===========================

This project contains the source code required to run simulations
on the *average edit distance* between random strings of the same
length.

Usage
-----


````
        aed [task] [options]

Allowed task:
  [omitted]:      Computes the edit distance using the selected algorithm
  cost-f:         Computes the difference in cost due to mutations
  min-spread-f:   Computes the difference in minimum spread due to mutations
  wide-alphabet:  Computes the edit distance using wide alphabets (--alphabet integer needed)

All options:
  -n [ --length ] arg    Size first string (if m is specified) or both string 
                         otherwise
  -k [ --samples ] arg   Number of samples (e.g., string pairs) to perform
  -a [ --algorithm ] arg Algorithm used to perform simulation: 0=WF (default), 
                         1=LinBand, 2=LinInfo
  -w [ --bandwidth ] arg Value of the bandwidth (equals n if not specified)
  --alphabet arg         Alphabet to be used for random symbol generation 
                         (default 'ACGT')
  -s [ --seed ] arg      Seed for random generations
  -v [ --verbosity ] arg Verbosity level of the program output (integer)
  -h [ --help ]          Show help message

```

Citing
------
If you are using this software consider citing our paper

Schimd, M., & Bilardi, G. (2019, October). Bounds and Estimates on the Average Edit Distance. *In International Symposium on String Processing and Information Retrieval* (pp. 91-106). Springer, Cham.
https://doi.org/10.1007/978-3-030-32686-9_7

