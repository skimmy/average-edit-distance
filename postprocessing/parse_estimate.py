# Copyright 2020 Michele Schimd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Parses the output of running 'aed' using the linear space
info algorithm (options -a 2). It takes the file and n
(string size) as input and produces several metric and
plots as output. An optional third argument indicates the
output directory for various files (e.g., plots).
"""

import sys
import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()


def Q(n, A=1.0, F=1.0):
    return F / (n - 1) + A * np.sqrt((2.0 / (n - 1)) * ((float(n + 1)) / (float(n - 1)) + np.log(n - 1)))


if len(sys.argv) < 3:
    print("Usage:\n  {0} file.csv n [output dir]\n".format(sys.argv[0]))
    sys.exit(1)

df = pd.read_csv(sys.argv[1])
n = int(sys.argv[2])
k = len(df)

print("Info")
print("n =    {0}".format(n))
print("k =    {0}".format(k))
print()

print("Summary")
ed_ = df.columns[0]
count_ = df.columns[1]
min_s_ = df.columns[2]
max_s_ = df.columns[3]
min_sp_ = df.columns[4]
print("e(n)     = {0:.8f}".format(df[ed_].mean()))
print("S(n)     = {0:.8f}".format(df[ed_].std()))
print("Median   = {0}".format(df[ed_].median()))
print("Min      = {0:.8f}".format(df[ed_].min()))
print("Max      = {0:.8f}".format(df[ed_].max()))
print("Alpha(n) = {0:.8f}".format(df[ed_].mean() / n))
print("Hat(l)   = {0:.8f}".format(df[ed_].mean() / n - Q(n)/2))
print("MinAlpha = {0:.8f}".format(df[ed_].min() / n))
print("MaxAlpha = {0:.8f}".format(df[ed_].max() / n))
print("Min SUB  = {0:.8f}".format(df[min_s_].mean()))
print("Max SUB  = {0:.8f}".format(df[max_s_].mean()))
print("Count    = {0}".format(df[count_].mean()))

print()
print("Minimum Diagonal Spread")
print("Mean     = {0:.8f}".format(df[min_sp_].mean()))
print("Std.     = {0:.8f}".format(df[min_sp_].std()))
print("Min      = {0}".format(df[min_sp_].min()))
print("Max      = {0}".format(df[min_sp_].max()))
print("Err.     = {0:.8f}".format(df[min_sp_].std() / np.sqrt(k)))
print()

print("Confidence")
A = 1
eps = np.asarray([.5, .25, 0.05, 0.01, 0.001])
for e in eps:
    Delta = A * np.sqrt((np.log(2.0 / e)) / (k * n))
    q = Q(n) / 2
    alpha_tilde = df[ed_].mean() / n
    print("lambda={0:.3f}    Delta={1:.4E}    Q(n)/2={2:.8f}    R(n,N,Delta)={3:.8f}".format(1 - e, Delta, q, q + Delta))
    print("      alpha_tilde(n):  {0:.8f}    [{1:.8f}, {2:.8f}]".format(
        alpha_tilde,
        alpha_tilde - Delta,
        alpha_tilde + Delta)
    )
    print("      alpha_hat(n):    {0:.8f}    [{1:.8f}, {2:.8f}]".format(
        alpha_tilde - q,
        alpha_tilde - q - (Delta + q),
        alpha_tilde - q + (Delta + q))
    )


# If an output directory is given, produce the plots
if len(sys.argv) >= 4:
    out_dir = sys.argv[3]
    bins = 50
    fig, ax = plt.subplots()
    sns.histplot(df[ed_], bins=bins, ax=ax)
    ax.set_xlim([510,570])
    ax.set_xlabel("$d_E(\\mathbf{x}, \\mathbf{y})$")
    ax.set_ylabel("Frequency")
    plt.tight_layout()
    plt.savefig(os.path.join(out_dir, "distance_distribution.pdf"))
    plt.close()
    fix, ax = plt.subplots()
    sns.histplot(df[min_s_], bins=bins, label="Min. SUB", ax=ax)
    sns.histplot(df[max_s_], bins=bins, label="Max. SUB", color="C1", ax=ax)
    ax.set_xlim([150, 450])
    ax.set_ylim([0, 50000])
    ax.set_xlabel("Distance")
    ax.set_ylabel("Frequency")
    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(out_dir, "minmax_sub_distribution.pdf"))
    plt.close()
    # plt.semilogy(ns, Qs)
    # plt.tight_layout()
    # plt.savefig(os.path.join(out_dir, "Q_plot.pdf"))
    # print()
    # print("Q(n)")
    # ns = np.logspace(10, 20, base=2, num=11)
    # Qs = np.asarray([Q(n)/2.0 for n in ns])
    # for i in range(len(ns)):
    #     print("2^{0:3} Q(n)/2 = {1:.8f}".format(int(np.log2(ns[i])), Qs[i]))

