import sys

import pandas as pd

dist_ = "distance"
exact_ = "exact"
band_ = "bandwidth"
diff_ = "diff"


def main():
    if len(sys.argv) < 4:
        print("Usage:\n{0} {1}".format(sys.argv[0], "exact.csv bandwidth.csv n"))
        sys.exit(1)

    df_ed = pd.read_csv(sys.argv[1])
    df_bw = pd.read_csv(sys.argv[2])
    n = int(sys.argv[3])
    df = pd.DataFrame(index=df_ed.index)
    k = len(df)
    df[exact_] = df_ed[dist_]
    df[band_] = df_bw[dist_]
    df[diff_] = df[band_] - df[exact_]
    print()

    print("n   {0}".format(n))
    print("k   {0}".format(k))

    print()
    print("Ave(Err):   {0:.8f}".format(df[diff_].mean()))
    print("Std(Err):   {0:.8f}".format(df[diff_].std()))
    print("Min(Err):   {0}".format(df[diff_].min()))
    print("Max(Err):   {0}".format(df[diff_].max()))
    exact_count = len(df[df[diff_] == 0])
    print("Exact:      {0} ({1:.2f}%)".format(exact_count, exact_count / k * 100))
    print()
    df_err = df[df[diff_] > 0]
    print(df_err[diff_].value_counts())


if __name__ == '__main__':
    main()
