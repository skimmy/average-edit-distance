import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

ns = [32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16348, 32768, 65536, 131072, 262144]
means = [2.3, 4.11, 7.13, 11.54, 19.22, 31.43, 50.89, 82.27, 131.5, 214.2, 340.7, 555.3, 932.5, 1272]
stds = [1.28, 2.02, 3.28, 4.93, 7.99, 12.71, 20.20, 32.03, 51.08, 83.40, 130.2, 199.2, 419.7, 342.9]
errs = [0.04, 0.064, 0.104, 0.156, 0.026, 0.018, 0.056, 0.177, 0.564, 1.843, 5.753, 17.6, 74.2, 121]

# ns = [32, 64, 128, 256]
# means = []
# for n in ns:
#     mean = pd.read_csv(sys.argv[1] + str(n) + ".csv")["min_spread"].mean()
#     means.append(mean)
# print(means)

plt.errorbar(ns, means, yerr=[2*e for e in errs], fmt="-o", alpha=0.75, label="Means")
# plt.plot(ns, np.power(ns, 0.5), alpha=0.5, label="$n^{0.5}$")
# plt.plot(ns, np.power(ns * np.multiply(0.2, np.log(ns)), 0.5), alpha=0.5, label="$\\sqrt{n \\log n}$")
plt.plot(ns, np.multiply(0.25, np.power(ns, 0.694)), alpha=0.75, label="$0.25n^{0.694}$")
plt.legend()
plt.grid()
# plt.title("Comparison between minimum spread and various functions $n^\\eta$")
plt.xlabel("$n$")
plt.tight_layout()
plt.savefig("/tmp/min_spread_vs_square_root.pdf")
