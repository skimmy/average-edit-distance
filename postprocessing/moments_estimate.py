# Copyright 2020 Michele Schimd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Parses output from aed 'sample' task and produces various
statistics: mean, variance, rho (centralized absolute third
moment). If a second parameter is given, computes the estimate
for \alpha defined as the average values divided by n as
given in the second parameter.
"""
import sys

import numpy as np
import pandas as pd

file_name = sys.argv[1]

df = pd.read_csv(file_name)
name = df.columns[0]
sample_mean = df[name].mean()
sample_variance = df[name].var()
sample_abs_third_moment = np.sum(np.power(np.abs(df[name]-sample_mean), 3)) / len(df)
sample_rho = sample_abs_third_moment / np.power(sample_variance, 1.5)

print("Mean:  ", sample_mean)
print("Var:   ", sample_variance)
print("Rho:   ", sample_rho)

if len(sys.argv) > 2:
    n = int(sys.argv[2])
    print("Alpha: ", sample_mean / n)
